"use strict";
var Imap = require("imap");
const simpleParser = require('mailparser').simpleParser;
var Promise = require("bluebird");
const log = require('../helpers/log-config');
const { subDays, startOfDay, isValid, parse } = require('date-fns');
const da = require('../libs/data_access');
require('dotenv').config();
const mqttc = require('../helpers/mqttconf')

Promise.config({
    longStackTraces: true
})
module.exports = {
    start: da.getGetAllSetups
}
const IMAPS = [] //konfiguracje dla aktywnych srzynek
let ls = {} //zmienna zawierająca config

async function RunMain() {
    //await auth.getTokenAndSavetoDB(); //run once at startup of deamon!
    console.log(`RUN MAIN()`)
    ls = await da.getGetAllSetups()
    const mailboxes2scan = await da.getActiveMailboxes()
    return Promise.map(mailboxes2scan, mailbox => {
        return createConfigAndListen(mailbox)
    }, { concurrency: 1 })
}

async function createConfigAndListen(mailbox) {
    const { le_id = null } = mailbox || {}
    return createIMapConfig(mailbox).then(imapConf => {
        const imap = new Imap(imapConf)
        console.log(`[START!]: Nasłuch na koncie ${imapConf.user} ; host: ${imapConf.host} `)

        imap.once("error", function (err) {
            console.error('[CONN_ERROR]:', err)
            log.error("Connection error: " + err.stack);
        });
        imap.on('mail', (numNewMsgs) => {
            // console.log("NEW MAIL Callback- messages: " + numNewMsgs);
            console.log(`imap.on('mail')`)
            openAndSearchAndProcess(imap, le_id)
        });
        imap.once('closed', function (numNewMsgs) {
            console.log("IMAP CLOSED: " + numNewMsgs);
        });
        imap.once('end', function (numNewMsgs) {
            console.log(`IMAP ENDED: ${numNewMsgs || ''}`);
        });
        imap.once("ready", function () {
            console.log(`IMAP READY:`);
            openOnlyMailbox(imap)
        });
        imap.connect();
        IMAPS.push(imap)
    })
}

RunMain()

async function createIMapConfig(mailbox) {
    let conf = {
        user: mailbox.user_login,
        password: mailbox.user_p,
        host: mailbox.host_url,
        port: mailbox.imap_port,
        authTimeout: mailbox.imap_auth_timeout,
        le_id: mailbox.le_id,
        tls: mailbox.imap_tls
    }
    conf = {
        ...conf, ...{
            tlsOptions: {
                rejectUnauthorized: !!mailbox.imap_reject_unauthorized
            }
        }
    }

    return conf
}
async function createIMapConfigNew(mailbox) {

    let conf = {
        user: mailbox.user_login,
        //password: mailbox.user_p,
        host: mailbox.host_url,
        port: mailbox.imap_port,
        authTimeout: mailbox.imap_auth_timeout,
        le_id: mailbox.le_id,
        tls: mailbox.imap_tls
    }
    conf = { ...conf, ...{ tlsOptions: { rejectUnauthorized: !!mailbox.imap_reject_unauthorized } } }
    return conf
}


//IMAP metody
function openOnlyMailbox(imap) {
    imap.openBox("INBOX", false, function (err, mailBox) {
        if (err) {
            console.error('[Error Opening Mailbox', err);
            return;
        }
        const mailbx = mailBox
    });
}
function openAndSearchAndProcess(imap, le_id) {
    imap.openBox("INBOX", false, function (err, mailBox) {
        if (err) {
            console.error('[Error Opening Mailbox', err);
            return;
        }
        const dateFrom = startOfDay(subDays(new Date, ls.days_back2check))
        imap.search(['ALL', ['SINCE', dateFrom]], function (err, results) {
            const EMAIL_COUNT = results.length || 0
            if (!results || !results.length) {
                console.log("No unread mails");
                //imap.end();
                return;
            }
            //identyfikacja z której skrzynki ida dane
            const { _config = null } = imap || {}
            const { user = null, host = null } = _config || {}
            const mbid = { user, host }
            const fetchOptions = { bodies: "" } //{ bodies: ['HEADER'], struct: true };

            const fetch = imap.fetch(results, fetchOptions);
            let PROCCNT = 0
            //zmiana 5.06.022 - funkcja anonimowa aby dostarczy le_id do zapisu w nowym dokumencie...
            fetch.on("message", (msg, seqno) => {
                msg.on("body", (stream) => {
                    simpleParser(stream, (err, mail) => {
                        if (err) return { error: true, err_o: err }
                        PROCCNT++
                        ProcessEmail(mail, seqno, le_id)
                        //console.log(`[MOCK!!!! ] - simulation processing without actual processing`)
                    });
                });
                msg.once("end", () => {
                    console.log("Done parsing msg #" + seqno);
                });
            });

            fetch.once("error", function (err) {
                console.error('[FETCH ERROR]: ' + err);
                throw err
            });
            fetch.once("end", function () {
                console.log(`[OD FETCH END] Emails processed/ALL: ${PROCCNT}/${EMAIL_COUNT}`);
                //  imap.end();
                if (PROCCNT)
                    mqttc.publish('se/od/added', `Otrzymano nowe dokumenty.`)
            });
        });
    });
}

// MAIN: główny entry point do przetwarzania emaila po jego parsowaniu
async function ProcessEmail(mail, seqno, le_id = null) {
    const { messageId = null, html = '', text = '', subject = '', attachments = [] } = mail || {}

    //test czy importowac email bez załaczników
    const importwAttsOnly = ls.import_only_mails_w_att
    if (importwAttsOnly && attachments.length === 0) {
        console.log(`[${seqno}][EMAIL bez załącznika] email pominięty z uwagi na ustawienia globalne!`)
        return
    }
    const rkw = Check4KeyWords(mail)
    if (!rkw.ok) {
        console.log(`[${seqno}][NOT PROCESSING] ${mail.subject}:  `, rkw.msg)
        return
    }
    const isProcessed = await da.testIfMAilAlreadyinOD(messageId)
    if (isProcessed) {
        console.log(`[${seqno}][EMAIL already processed in OD]: ${mail.messageId}`)
        return
    }

    console.log(`[${seqno}][STEP 3] for ${mail.messageId}`)
    const def_usr = ls.def_user
    const od_sett = ls.od_settings
    const od_doc = await da.CreateNewInvoiceDocument(mail, od_sett, def_usr, le_id);
    if (!od_doc) {
        console.log(`[ERROR Creating OD document] from email: ${mail.messageId}`)
        return
    }

    //TODO wczytwanie hyperlinków
    //const urlRegex = /^https?:\/\/.*?\/([a-zA-Z-_]+).*$/;
    // const urls = html.match(urlRegex);
    //console.log(urls)

    const ret_save = await da.SaveEmailInvoice2db(od_doc, attachments, def_usr,)
    return ret_save
}


function safeConvert2Date(in_datetime) {
    //na wejściu data lub string daty  - na wyjsciu zawsze data
    if (in_datetime === null || in_datetime === undefined)
        return null
    let s_dt = null;
    if (typeof in_datetime === 'string') { //zamiana łacucha na datę
        s_dt = parse(in_datetime);
    } else {
        s_dt = in_datetime //bez konwersji
    }
    if (isValid(s_dt))
        return s_dt
    else
        return null
}




function Check4KeyWords(mail) {
    const { messageId = null, subject = '', } = mail || {}
    //test czy Subject zawiera słowa klucze do akceptacji jako faktura
    const lCaseSub = subject.toLowerCase();
    const hasACCKeyWords = ls.KEYWORDS_ACC.find((keyword) => {
        const indx = lCaseSub.indexOf(keyword)
        return indx >= 0
    })
    if (!hasACCKeyWords) return { ok: false, msg: 'No keywords found in Subject (' + lCaseSub + ')' }
    //test czy są słowa wykluczajace
    const hasIGNOREKeyWords = ls.KEYWORDS_IGN.find((keyword) => {
        const indx = lCaseSub.indexOf(keyword)
        return indx >= 0
    })
    if (!!hasIGNOREKeyWords) return { ok: false, msg: 'Ignore keywords detected in Subject (' + lCaseSub + ')' }

    return { ok: true, msg: `Email ${messageId} OK 4 processing`, mail: mail }
}


