"use strict";
var Imap = require("imap");
const simpleParser = require('mailparser').simpleParser;
var Promise = require("bluebird");
const log = require('../helpers/log-config');
const { subDays, startOfDay, isValid, parse } = require('date-fns');
const da = require('../libs/data_access');
require('dotenv').config();
const mqttc = require('../helpers/mqttconf')
const appset = require('../libs/app_settings')

Promise.config({
    longStackTraces: true
})
module.exports = {
    start: da.getSetup4ServiceIncoming
}
const IMAPS = [] //konfiguracje dla aktywnych srzynek
let ls = {} //zmienna zawierająca config

RunMain()

async function RunMain() {
    ls = await da.getSetup4ServiceIncoming()
    const mailboxes2scan = await da.getActiveMailboxes4ServiceCalls()
    if (!mailboxes2scan || mailboxes2scan.length === 0) {
        console.log(`[END]: Brak skonfigurowanych skrzynek zaznaczonych do odbierania zleceń!`)
        return
    }
    return Promise.map(mailboxes2scan, mailbox => {
        return createConfigAndListen(mailbox)
    }, { concurrency: 1 })
}

async function createConfigAndListen(mailbox) {
    const { le_id = null } = mailbox || {}
    return createIMapConfig(mailbox).then(imapConf => {
        const imap = new Imap(imapConf)
        console.log(`[START!]: Nasłuch na koncie ${imapConf.user} ; host: ${imapConf.host} `)

        imap.once("error", function (err) {
            const { source, textCode, message, stack } = err || {}
            console.error(`[CONNECTION_ERROR]: SRC: ${source}; TXTCODE: ${textCode}; MSG: ${message} `)
            log.error(`Connection error: ` + err.stack);
        });
        imap.on('mail', (numNewMsgs) => {
            // console.log("NEW MAIL Callback- messages: " + numNewMsgs);
            openAndSearchAndProcess(imap, le_id)
        });
        imap.once('closed', function (numNewMsgs) {
            console.log("IMAP CLOSED: " + numNewMsgs);
        });
        imap.once('end', function (numNewMsgs) {
            console.log(`IMAP ENDED: ${numNewMsgs || ''}`);
        });
        imap.once("ready", (args) => {
            console.log(`IMAP READY:`, imapConf);
            openOnlyMailbox(imap)
        });
        imap.connect();
        IMAPS.push(imap)
    })
}



async function openAndSearchAndProcess(imap, le_id) {
    const openedMailbox = await ImapOpenInbox(imap)
    const fromDate = startOfDay(subDays(new Date, ls.days_back2check))
    const results = await ImapSearchInbox(imap, fromDate)
    const EMAIL_COUNT = results.length || 0
    let PROCCNT = 0
    if (!results || !results.length) {
        console.log("No unread mails");
        imap.end();
        return;
    }
    //identyfikacja z której skrzynki ida dane
    const { _config = null } = imap || {}
    const { user = null, host = null } = _config || {}
    const mbid = { user, host }
    const fetchOptions = { bodies: "" } //{ bodies: ['HEADER'], struct: true };
    const fetch = imap.fetch(results, fetchOptions);
    //zmiana 5.06.022 - funkcja anonimowa aby dostarczy le_id do zapisu w nowym dokumencie...
    fetch.on("message", (msg, seqno) => {
        msg.on("body", (stream) => {
            simpleParser(stream, (err, mail) => {
                if (err) return { error: true, err_o: err }
                PROCCNT++
                ProcessEmail(mail, seqno, le_id)
            });
        });
        msg.once("end", () => {
            console.log("Done parsing msg #" + seqno);
        });
        msg.once('attributes', function (attrs) {
            // console.log(`[Attributes]`, attrs);
        });
        msg.once('error', function (errrrr) {
            console.log(`[MSG FETCH ERROR]`, errrrr);
        });
    });

    fetch.once("error", function (err) {
        console.error('[FETCH ERROR]: ' + err);
        throw err
    });
    fetch.once("end", function () {
        console.log(`[SE CALLS FETCH END] Emails processed/ALL: ${PROCCNT}/${EMAIL_COUNT}`);
        //  imap.end();
        if (PROCCNT)
            mqttc.publish('se/secalls/added', `Otrzymano nowe e-maile zleceń ${PROCCNT}.`)
    });
}

// MAIN: główny entry point do przetwarzania emaila po jego parsowaniu
async function ProcessEmail(mail, seqno, le_id = null) {
    const { messageId = null, html = '', text = '', subject = '', attachments = [] } = mail || {}
    const def_usr = ls.def_user
    const call_sett = ls.call_settings
    const { en_cont_list } = call_sett || {}
    const em = da.convEmailObj2HumReadble(mail)//uproszcozny obiekt emaila
    const { email_uuid } = em || {}
    const exist = await da.checkIfEmailSaved(email_uuid)
    if (exist) {
        const { call_id, email_date } = exist || {}
        console.log(`EMAIL ${email_uuid} był już zapisany jako zlecenie nr ${call_id}.`)
        return
    }
    const zlecSetup = await appset.getOptions4Zlecenia()
    const { seIncomEmailIgnoreFilters = false } = zlecSetup || {}
    const find_result_o = await da.findClientAndSiteByEmail(em)
    const { contact_a, client_o, site_o, site_id, client_id, is_client, cli_imap_enabled } = find_result_o || {}

    if (!seIncomEmailIgnoreFilters) {
        //test czy importowac email bez załaczników
        const importwAttsOnly = ls.import_only_mails_w_att
        if (importwAttsOnly && attachments.length === 0) {
            console.log(`[${seqno}][EMAIL bez załącznika] email pominięty z uwagi na ustawienia globalne!`)
            return
        }
        const rkw = Check4KeyWords(mail)
        if (!rkw.ok) {
            console.log(`[${seqno}][NOT PROCESSING] ${mail.subject}:  `, rkw.msg)
            return
        }
        const isProcessed = await da.testIf_SECALL_MailAlreadySaved(messageId)
        if (isProcessed) {
            console.log(`[${seqno}][EMAIL already processed in OD]: ${mail.messageId}`)
            return
        }
        console.log(`[${seqno}][STEP 3] for ${mail.messageId}`)

        if (!is_client) {
            console.log(`[CONFIG] Sender ${em.fromEmail} is Not identified as client. Aborting ... `)
            return
        }
        if (!cli_imap_enabled) {
            console.log(`[CONFIG] Client IMAP_EMAIL_RECEIVE flag not SET! Aborting mail from sender: ${em.fromEmail}`)
            return
        }
    }

    const se_call = await da.CreateNew_SE_CallCall(em, find_result_o, call_sett, def_usr, le_id);
    if (!se_call) {
        console.log(`[ERROR Creating SE CALL document] from email: ${mail.messageId}`)
        return
    }
    const ret_save = await da.SaveSE_Call_2_DB(se_call, em, attachments, def_usr,)
    return ret_save
}


function Check4KeyWords(mail) {
    const { messageId = null, subject = '', } = mail || {}
    //test czy Subject zawiera słowa klucze do akceptacji jako faktura
    const lCaseSub = subject.toLowerCase();
    const hasACCKeyWords = ls.KEYWORDS_ACC.find((keyword) => {
        const indx = lCaseSub.indexOf(keyword)
        return indx >= 0
    })
    if (!hasACCKeyWords)
        return { ok: false, msg: 'No keywords found in Subject (' + lCaseSub + ')' }

    //test czy są słowa wykluczajace
    const hasIGNOREKeyWords = ls.KEYWORDS_IGN.find((keyword) => {
        const indx = lCaseSub.indexOf(keyword)
        return indx >= 0
    })
    if (!!hasIGNOREKeyWords) return { ok: false, msg: 'Ignore keywords detected in Subject (' + lCaseSub + ')' }

    return { ok: true, msg: `Email ${messageId} OK 4 processing`, mail: mail }
}

// xoauth - string - Base64 - encoded OAuth token for OAuth authentication for servers that support it(See Andris Reinman's xoauth.js module to help generate this string).
// xoauth2 - string - Base64 - encoded OAuth2 token for The SASL XOAUTH2 Mechanism for servers

async function createIMapConfig(mailbox) {
    let conf = {
        user: mailbox.user_login,
        password: mailbox.user_p,
        host: mailbox.host_url,
        port: mailbox.imap_port,
        authTimeout: mailbox.imap_auth_timeout,
        le_id: mailbox.le_id,
        tls: mailbox.imap_tls
    }
    conf = {
        ...conf, ...{
            tlsOptions: {
                rejectUnauthorized: !!mailbox.imap_reject_unauthorized
            }
        }
    }

    return conf
}


//IMAP metody
function openOnlyMailbox(imap) {
    imap.openBox("INBOX", false, function (err, mailBox) {
        if (err) {
            console.error('[Error Opening Mailbox', err);
            return;
        }
        const mailbx = mailBox
    });
}

function ImapOpenInbox(imap) {
    return new Promise(function (resolve, reject) {
        imap.openBox("INBOX", false, (err, mailBox) => {
            if (err)
                reject(err);
            else
                resolve(mailBox)
        })
    });
}
function ImapSearchInbox(imap, dateFrom) {
    return new Promise(function (resolve, reject) {
        imap.search(['ALL', ['SINCE', dateFrom]], function (err, results) {
            if (err)
                reject(err);
            else
                resolve(results)
        })
    });
}
