"use strict";
require('dotenv').config();
const pg = require('pg')
pg.types.setTypeParser(1700, 'text', parseFloat)

const Databse = {
    client: 'pg',
    connection: {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        port: process.env.DB_PORT,
    },
    debug: false,
};
console.log(`KNEX using env variables:  ${process.env.DB_HOST}:${process.env.DB_PORT}`)
const knex = require('knex')(Databse);
module.exports = knex;

