"use strict";
const mqtt = require('mqtt')
const appSet = require('../libs/app_settings')

let client
let cc_prexix = ''
const hostname = process.env.MQTT_HOST
const port = process.env.MQTT_PORT
const protocol = process.env.MQTT_PROTOCOL
const username = process.env.MQTT_USER
const password = process.env.MQTT_PASSWORD

initMqqtClient()

module.exports = {
    client: client,
    cc_prexix: cc_prexix,
    publish: publish
}
async function publish(topic, message) {
    const fulltopic = `/${cc_prexix}/${topic}`
    if (!client) {
        return {}
    }
    client.publish(fulltopic, message)
}
async function initMqqtClient() {
    const deep = await appSet.getDeepSettings()
    const { mqtt_cc } = deep || {} //prefix firmy (instancji SE) - unikalny i iniezbedny do sukskrypcji
    cc_prexix = mqtt_cc
    if (protocol && hostname && port && mqtt_cc) {
        const connectUrl = `${protocol}://${hostname}:${port}`
        const opts = {
            clientId: `cli_mqtt_${mqtt_cc}_${Math.random().toString(16).slice(3)}`,
            clean: true,
            connectTimeout: 4000,
            username: username,
            password: password,
            reconnectPeriod: 1000,
        }
        client = mqtt.connect(connectUrl, opts)

        console.log(`[MQTT Config] Initialized mqtt client connection to: ${hostname}:${port}`)
        client.on('connect', function () {
            client.subscribe('presence', function (err) {
                if (!err) {
                    client.publish('presence', 'On connect Hello mqtt')
                    client.publish(`/${cc_prexix}/test`, `MQTT API client started from API. Instance database: ${process.env.DB_NAME}`)
                }
            })
        })
        client.on('message', function (topic, message) {
            // message is Buffer
            console.log('[CLIENT ON MESSAGE]', message.toString())
            //client.end()
        })
    } else {
        console.warn(`[MQTT Config] Invalid config options! Check api.config file and deep settings (cc: ${mqtt_cc})!`)
    }

    return client
}




