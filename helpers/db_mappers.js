"use strict";
module.exports = {

    map4db_fdoc_emails: function (dto) {
        return {
            id: dto.id, parent_id: dto.parent_id, date_received: dto.date_received, m_from: dto.m_from, m_to: dto.m_to,
            m_subject: dto.m_subject, m_text: dto.m_text, m_attachments: dto.m_attachments, email_uuid: dto.email_uuid
        }
    },
    map4db_fdoc_documents: function (dto) {
        return {
            id: dto.id, uuid: dto.uuid, date_added: dto.date_added, added_by: dto.added_by,
            type_id: dto.type_id, status_id: dto.status_id, legal_ent_id: dto.legal_ent_id,
            bunit_id: dto.bunit_id, b_partner_id: dto.b_partner_id, bp_name: dto.bp_name,
            bp_address: dto.bp_address, bp_zip: dto.bp_zip, bp_city: dto.bp_city, vatid: dto.vatid,
            fa_number: dto.fa_number, fa_date: dto.fa_date, fa_sales_date: dto.fa_sales_date, fa_cost_month: dto.fa_cost_month,
            date_received: dto.date_received, net_value: dto.net_value, vat_amount: dto.vat_amount, curr: dto.curr, opt_field1: dto.opt_field1,
            opt_field2: dto.opt_field2, opt_field3: dto.opt_field3, opt_field4: dto.opt_field4, dcomments: dto.dcomments, mpk_id: dto.mpk_id,
            gl_account_id: dto.gl_account_id, einvoice: dto.einvoice, gross_value: dto.gross_value, call_id: dto.call_id, is_booked: dto.is_booked,
            book_date: dto.book_date, book_ref: dto.book_ref, le_prefix: dto.le_prefix, custom_doc_id: dto.custom_doc_id, has_optdata: dto.has_optdata,
            accountant_id: dto.accountant_id, use_split: dto.use_split, sender_email: dto.sender_email, email_uuid: dto.email_uuid, from_imap:
                dto.from_imap, customno: dto.customno, last_changed: dto.last_changed, last_changed_by: dto.last_changed_by, email_date: dto.email_date,
            is_proforma: dto.is_proforma, cost_cat_id: dto.cost_cat_id, legacy_mpk: dto.legacy_mpk, erp_po_no: dto.erp_po_no, project_id: dto.project_id,
            use_call_split: dto.use_call_split, payment_due_date: dto.payment_due_date, payment_done: dto.payment_done, fa_cost_year: dto.fa_cost_year
        }
    },
    map4db_fdoc_acceptance: function (dto) {
        return {
            id: dto.id, uuid: dto.uuid, added_by: dto.added_by, dateadded: dto.dateadded, invoice_id: dto.invoice_id, acceptor_id: dto.acceptor_id,
            accepted: dto.accepted, acceptor_decision_date: dto.acceptor_decision_date, remarks: dto.remarks, step: dto.step
        }
    },
    map4db_se_service_calls: function (dto) {
        return {
            last_updated_by: dto.last_updated_by,
            date_registered: dto.date_registered,
            added_by: dto.added_by,
            call_type_id: dto.call_type_id,
            team_leader_id: dto.team_leader_id,
            site_id: dto.site_id,
            asset_id: dto.asset_id,
            use_html: dto.use_html,
            client_visit_plan_email: dto.client_visit_plan_email,
            our_priority: dto.our_priority,
            last_updated: dto.last_updated,
            id: dto.id,
            fmid: dto.fmid,
            block_protocoltime_display: dto.block_protocoltime_display,
            opiekun_id: dto.opiekun_id,
            bu_id: dto.bu_id,
            system_type_id: dto.system_type_id,
            dev_type_id: dto.dev_type_id,
            sap_wbs_id: dto.sap_wbs_id,
            sap_project_id: dto.sap_project_id,
            le_id: dto.le_id,
            block4erp: dto.block4erp,
            transfr_pending: dto.transfr_pending,
            transf_time: dto.transf_time,
            fault_type_id: dto.fault_type_id,
            maintnce_intrv_id: dto.maintnce_intrv_id,
            ppm_id: dto.ppm_id,
            oferta_id: dto.oferta_id,
            lob_id: dto.lob_id,
            status_id: dto.status_id,
            date_expected_by_customer: dto.date_expected_by_customer,
            deadline_internal: dto.deadline_internal,
            client_id: dto.client_id,
            client_notifyvia_email: dto.client_notifyvia_email,
            client_notifyvia_sms: dto.client_notifyvia_sms,
            client_is_billto: dto.client_is_billto,
            billto_id: dto.billto_id,
            notify_techn_v_email: dto.notify_techn_v_email,
            notify_techn_v_sms: dto.notify_techn_v_sms,
            vendorclaimreqired: dto.vendorclaimreqired,
            revenue_expected: dto.revenue_expected,
            cost_expected: dto.cost_expected,
            date_invoicing_request: dto.date_invoicing_request,
            invoice_sent_quotation_with_invoice: dto.invoice_sent_quotation_with_invoice,
            rez_gwa_wartosc: dto.rez_gwa_wartosc,
            invoice_date: dto.invoice_date,
            cost_center_id: dto.cost_center_id,
            condit_lbr_per_hour: dto.condit_lbr_per_hour,
            date_client_submission: dto.date_client_submission,
            client_priority: dto.client_priority,
            client_type: dto.client_type,
            portal_origin: dto.portal_origin,
            seller_id: dto.seller_id,
            client_call_recepit_email: dto.client_call_recepit_email,
            technician_origin: dto.technician_origin,
            imap_origin: dto.imap_origin,
            client_caller_id: dto.client_caller_id,
            erp_reference: dto.erp_reference,
            lcc_id: dto.lcc_id,
            site_location: dto.site_location,
            rez_gwa_powod: dto.rez_gwa_powod,
            invice_no: dto.invice_no,
            uuid: dto.uuid,
            client_order_no: dto.client_order_no,
            sap_order_no: dto.sap_order_no,
            client_name: dto.client_name,
            client_adress: dto.client_adress,
            client_city: dto.client_city,
            client_caller_name: dto.client_caller_name,
            client_caller_telephone: dto.client_caller_telephone,
            client_email: dto.client_email,
            coordinator_memo: dto.coordinator_memo,
            invoice_instructions: dto.invoice_instructions,
            kind: dto.kind,
            rez_gwar_typ: dto.rez_gwar_typ,
            client_issue_description: dto.client_issue_description,
            internal_memo: dto.internal_memo
        }
    }
}