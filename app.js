"use strict";
const express = require('express');
const logger = require('./helpers/log-config');
const { transports } = require('winston');
const app = express();
require('dotenv').config();
const ENV = app.get('env');

function exitHandler(options, exitCode) {
    if (options.cleanup) console.log('[EMAIL READER] exitHandler -  clean');
    if (exitCode || exitCode === 0) console.log('[EMAIL READER] exitHandler - exit CODE:', exitCode);
    if (options.exit) process.exit();
}
//WINSTON LOG
logger.exceptions.handle(new transports.File({ filename: 'logs/exceptions.log' }));

process.stdin.resume();//so the program will not close instantly
//do something when app is closing
process.on('exit', exitHandler.bind(null, { cleanup: true }));
process.on('SIGINT', exitHandler.bind(null, { exit: true }));  //catches ctrl+c event
process.on('SIGUSR1', exitHandler.bind(null, { exit: true }));// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR2', exitHandler.bind(null, { exit: true }));
process.on('uncaughtException', function (e) {
    console.error('[EMAIL READER]: Uncaught exception] error is: %s and stack trace is: %s', e, e.stack);
    console.log("[EMAIL READER]: Uncaught exception] Process will restart now - process.(exit(1).");
    process.exit(1);//wyjście z kodem 1 =  Uncaught Fatal Exception:
})


console.log(`Current ENV: ${ENV}; PG DATABASE: [${process.env.DB_NAME}] on [${process.env.DB_HOST}]`)
//node error handler
if (ENV === 'development') {
    app.use(function (err, req, res, next) {
        logger.error(err);
        let erO = { message: err.message, msg: err.message, error: JSON.stringify(err) }
        res.status(err.status || 500).json(erO);
    });
}
// production error handler // no stacktraces leaked to user
if (ENV === 'production') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.status(err.status || 500).json({ message: err.message });
        logger.error(err);
    });
}


//////////////////////////////////////////////////////////////////////////////
//START 2 modułów czytających emaile ze skrzynek (czytanych z konfiguracji) //
//////////////////////////////////////////////////////////////////////////////

//const seincomung = require('./imap_services/imap_zlecenia_serv_reader') //zlecenia serwisowe

const odrepo = require('./imap_services/imap_mail_reader_new')  //skrzynka z fakturami dostawów


//SERVER EXPRESS
const PORT = process.env.EXPR_PORT
const server = app.listen(PORT, function () {
    const memo = `[EMAIL READER APP] START ${new Date()} PORT: ${PORT} ...`
    console.log(memo)
    console.log(`[ENV VARS]: DB:${process.env.DB_NAME}, DBPORT: ${process.env.DB_PORT}, FILE_PATH: ${process.env.UPLOAD_PATH} `)

})
module.exports = server