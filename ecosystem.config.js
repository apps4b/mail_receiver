module.exports = {
  apps: [
    {
      name: 'ds_mail_receiver_od',
      script: 'app.js',
      args: '',
      exec_mode: "fork",
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development'
      },
      env_production: {
        NODE_ENV: 'production'
      }
    }
  ]
};
