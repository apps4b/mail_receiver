"use strict";
const db = require("../helpers/db");
module.exports = {
    getSetting4DInvoices: async function () {
        const bb = await getBaseOptionsNew()
        const { dinvoices } = bb || {}
        return dinvoices
    },
    getDeepSettings: getDeepSettingsAll,
    getOptions4Zlecenia: async function () {
        const bb = await getBaseOptionsNew()
        const { secalls } = bb || {}
        return secalls;
    },
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////    nowe metody ustawień o 21.08.2023  ////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
const newMainSel = ["id", "name", "uuid", "data_new"];
//main app settings -  NOWE od 21.08.2023
async function getNewMainSettings() {
    const row = await db("app_options").select(newMainSel).where('name', 'setup').first()
    return row;
}
//deep settings1 - tylko dla serwisu producenta
async function getNewDs1Settings() {
    const row = await db("app_options").select(newMainSel).where('name', 'ds1').first()
    return row;
}
//deep settings2 (nowe 08.2023 - nop ustawienia map są tutaj) - tylko serwis producenta
async function getNewDs2Settings() {
    const row = await db("app_options").select(newMainSel).where('name', 'ds2').first()
    return row;
}
async function getDeepSettingsAll() { //dla kopatybilnosci z wczesneijszym kodem (jest wioele wywołań getDeepSettings)
    const ds1 = await getNewDs1Settings()
    const ds2 = await getNewDs2Settings()
    const { data_new: dn1 } = ds1 || {}
    const { data_new: dn2 } = ds2 || {}
    const ret = { ...dn1, ...dn2 }
    return ret;
}
async function getBaseOptionsNew() {
    const base = await getNewMainSettings()
    const { data_new = {} } = base || {}
    return data_new;
}


