"use strict";
var Promise = require("bluebird");
const db = require("../helpers/db");
const { v4: fuid } = require('uuid');
const writeFileAsync = Promise.promisify(require("fs").writeFile);
const { getYear, parse, subDays, startOfDay, subMonths, isValid, format } = require('date-fns');
const libmime = require('libmime');
const setup = require('./app_settings');
const mappers = require('../helpers/db_mappers')
const sanitize = require("sanitize-filename");


module.exports = {
    CreateNewInvoiceDocument: CreateNewInvoiceDocument,
    CreateNew_SE_CallCall: CreateNew_SE_CallCall,
    findClientAndSiteByEmail: findClientAndSiteByEmail,
    SaveEmailInvoice2db: SaveEmailInvoice2db,
    SaveSE_Call_2_DB: SaveSE_Call_2_DB,
    getActiveMailboxes: getActiveMailboxes,
    getActiveMailboxes4ServiceCalls: getActiveMailboxes4ServiceCalls,
    testIfMAilAlreadyinOD: testIfMAilAlreadyinOD,
    testIf_SECALL_MailAlreadySaved: testIf_SECALL_MailAlreadySaved,
    getGetAllSetups: getGetAllSetups,
    getSetup4ServiceIncoming: getSetup4ServiceIncoming,
    dbTokenIsValid: dbTokenIsValid,
    convEmailObj2HumReadble: convEmailObj2HumReadble,
    checkIfEmailSaved: checkIfEmailSaved

}
async function dbTokenIsValid(app_key) {
    // const auth_token = await db('auth_tokens').where('appkey', app_key).first()
    // if (!auth_token)
    //     return false
    // const { token_string } = auth_token || {}
    // const needRefresh = auth.nearExpiryDate(token_string)
    // return !needRefresh
    return false
}
async function CreateNewInvoiceDocument(mailObj, od_settings, user, le_id) {
    const { detect_client = false, domainIgnoreKeywords = '', statdet_period = null,
        statdet_le = null, statdet_lev_le = null, statdet_bu = null, statdet_lev_bu = null,
        statdet_wbs = null, statdet_lev_wbs = null, statdet_cg = null, statdet_lev_cg = null } = od_settings || {};
    const { id: user_id = null } = user || {}
    const em = convEmailObj2HumReadble(mailObj)

    if (!em.email_uuid) {
        console.log('[BRAK ID maila!]')
        return null
    }
    const ignore_domains_arr = (domainIgnoreKeywords || '').split(",").map(s => s.toLowerCase()).map(s => s.trim()).filter(word => !!word);
    const isIgnoredDomain = ignore_domains_arr.find(d => {
        const indx = d.indexOf(em.from_domain)
        return (indx >= 0)
    })
    const detCli = !!detect_client && !isIgnoredDomain;
    const teraz = new Date();
    const year = getYear(teraz)
    const invoice = {
        type_id: null, date_added: teraz, date_received: teraz, added_by: user_id, status_id: 100,
        uuid: fuid(), einvoice: true, from_imap: true, mpk_id: null, legacy_mpk: false, curr: 'PLN',
        fa_cost_year: year || null, akceptacje: [], sender_email: em.from, email_uuid: em.email_uuid, email_date: em.date,
        emailobject: em /// dołaczony cały obiekt HReadable emaila
    }
    //data początkowa detekcji dokumentów
    const date_pocz = subMonths(new Date(), statdet_period || 0)
    //szukanie domyślnego typu dokumentu
    const dtyp = await db('fdoc_types').where("is_default", true).first()
    if (dtyp) {
        invoice.type_id = dtyp.id;
    }

    //ANALIZA załaczonego pliku JSON do faktury (matadata)
    const jsonMetaAnalyseResults = await getProcessInvoiceFromJSONMetaData(mailObj, user)
    if (jsonMetaAnalyseResults) {
        const ret_invoice = { ...invoice, ...jsonMetaAnalyseResults }
        return ret_invoice
    }

    /////////////// OPCAJ - anaiza historii emailii od nadawcy i wyszukwianie prawdopodobnych qartości

    ////////  ANALIZA na podstawie historiii ////////////////////
    //szukanie top W
    if (statdet_wbs) await checkAndFillField('mpk_id', date_pocz, em.from, statdet_lev_wbs, invoice)
    if (statdet_cg) await checkAndFillField('cost_cat_id', date_pocz, em.from, statdet_lev_cg, invoice)
    //Przypisanie spółki do tworoznego dokumentu
    // podany do tej funkcji parametr le_id ma pierwszeństoww
    if (le_id) {
        invoice.legal_ent_id = le_id
    } else {
        //próba znalezienia spółki poprzez email na kßóry przyszła wiadomosć (niepewne - były tu błedy przed zmianami 05 czerwca 2022
        // gdy to był jedyny mechanizm okreslania spółki)
        if (em.to) { //skrzynka na którą przyszedł email
            const act_mailbox = await db('app_mailboxes').where("user_login", em.to).first()
            const { le_id: LEID = null } = act_mailbox || {}
            invoice.legal_ent_id = LEID || null//jeśli spółka przypisana do skrzynki to zawsze nadpisujemy id spółki dokumentu!
        }
    }
    // pole spółki może byc dalej zmodyfikowane jeśli statystyka pokazuje że emaile z tego adresu
    //często sa pksiegowane na inną spólkę
    if (statdet_le) await checkAndFillField('legal_ent_id', date_pocz, em.from, statdet_lev_le, invoice)
    if (statdet_bu) await checkAndFillField('bunit_id', date_pocz, em.from, statdet_lev_bu, invoice)
    if (statdet_wbs) {
        await checkAndFillField('mpk_id', date_pocz, em.from, statdet_lev_wbs, invoice)
        //jeśli jest mpk_id to znajdowanie matrycy akceptacji...
        //znajdowanie matrycy akceptacji dla danego mpk_id (powinien być już tu w dokuemncie)
        if (invoice["mpk_id"]) {
            const mpk_id = invoice.mpk_id
            const akceptacjeArr = await getAcceptanceArray(invoice, user)
            invoice.akceptacje = akceptacjeArr || []
        }
    }
    //sprawdzanie ostatniej faktury z emailem od tego nadawcy - szukanie klienta ...
    const linv = await db('fdoc_documents').where('sender_email', em.from).orderBy('id', 'desc').first();
    if (linv && linv.b_partner_id && detCli) { //jest klient i chcemy wykonać detekcję
        const k = await cli_GetBusinessPartner(linv.b_partner_id);
        invoice.b_partner_id = linv.b_partner_id;
        invoice.bp_name = k.erp_name;
        invoice.bp_address = k.address1;
        invoice.bp_zip = '';
        invoice.bp_city = k.address2;
        invoice.vatid = k.tax_id;

    }
    return invoice
}
function convEmailObj2HumReadble(imapParsedMailObject) {
    const { date = null, headers = null, attachments = [], to = null, html = null, messageId: mid = null, subject = '', text = '', textAsHtml = '', from: fromHdr = {} } = imapParsedMailObject || {}
    const { text: from = null } = fromHdr || {}
    const { value: AddressArr } = to || {}
    let { address: to_Adress = '' } = AddressArr ? AddressArr[0] : ''
    const fromEmailArr = (from || '').split("@").map(s => s.toLowerCase()).map(s => s.trim()).filter(word => !!word);
    let senderDomain = fromEmailArr.length > 0 ? fromEmailArr[1] : ''
    senderDomain = senderDomain.replace(">", "")
    senderDomain = senderDomain.replace("<", "")
    const mail_uuid = mid ? mid.trim().replace(/\t/g, '') : ''

    let fromEmail = ''
    if (from) {
        const start = from.indexOf('<')
        const end = from.indexOf('>')
        if (start && end)
            fromEmail = from.substring(start + 1, end)
        else
            fromEmail = from
    }

    const ret_obj = {
        from: from,
        fromEmail: fromEmail,
        to: to_Adress,
        from_domain: senderDomain,
        date: date,
        subject: subject,
        html: html,
        text: text,
        textAshtml: textAsHtml,
        email_uuid: mail_uuid,
        att_info: ''
    }
    if (attachments && attachments.length > 0) {
        const att_txt = attachments.reduce((cumtxt, att) => {
            const { filename = '', size = null } = att || {}
            return cumtxt + (filename || '?') + ` (${size}B), `
        }, '')
        return { ...ret_obj, ...{ att_info: att_txt } }
    }
    return ret_obj
}


async function CreateNew_SE_CallCall(em, found_cli_site, zlec_sett, def_usr, le_id) {
    let call = getCallBaseTemplate();
    const le_o = await db(`fdoc_legal_ent`).select(db.raw(`id, entity_name`)).where('is_default', true).first()//domyślna spółka
    const type_o = await db('se_service_calls_types').where('is_default', true).first()
    const notification_flags = getNotifications(zlec_sett) //z ustawień systemowych defaultowe flagi powiadomień zlecenia
    const { en_cont_list } = zlec_sett || {}//czy zlecenei ma listę kontaktów
    const { subject, text, html, textAshtml, from, fromEmail, email_uuid } = em || {}
    const { contact_a, client_o, site_o, site_id, client_id, is_client, cli_imap_enabled } = found_cli_site || {}
    const { id: autor_id } = def_usr || {}
    if (!em.email_uuid) {
        console.log('[BRAK ID maila!]')
        return null
    }
    const subDate = safeConvert2Date(em.date)
    call.date_client_submission = subDate ? subDate : new Date()
    call.added_by = autor_id
    call = { ...call, ...notification_flags }
    call.use_html = true;
    call.imap_origin = true
    call.client_issue_description = `<div style="font-wight: 600">${subject}</div> ${html}`;
    call.call_type_id = type_o ? type_o.id : null
    call.le_id = le_o ? le_o.id : null
    //TODO
    if (site_id) call.site_id = site_id
    if (client_id) call.client_id = client_id

    if (contact_a && contact_a.length) { //jest lista kontaktów
        let { name, email, telefon } = contact_a[0]
        if (!en_cont_list) {
            call.client_caller_name = name;
            call.client_email = email;
            call.client_caller_telephone = telefon;
        } else {
            //tworzneie listy kontaktów jesli jest ta opcja włacozna 
            const ret_arr = contact_a.map(c => {
                const { id, name, email, telefon } = c || {}
                return { call_id: null, contact_id: id, name: name, phone: telefon, m4notify: false }
            })
            call.contacts_a = ret_arr
        }
    }
    //wyszukanie koordynatora z historii zleceń dla tego obiektu
    if (site_id) {
        const top_stat = await db('se_service_calls').select(db.raw(`team_leader_id ,count(*)`))
            .whereRaw(`(team_leader_id is not null) AND (site_id=:site_id)`, { site_id: site_id }).groupBy('team_leader_id').orderByRaw(`count(*) desc`).first()
        if (top_stat)
            call.team_leader_id = top_stat.team_leader_id
    }

    return call
}
function getCallBaseTemplate() {
    return {
        uuid: fuid(),
        date_registered: new Date(),
        imap_origin: true,
        use_html: false,
        status_id: 100,
        client_id: null,
        client_caller_name: null,
        client_caller_telephone: null,
        client_email: null,
        client_notifyvia_email: false,
        client_notifyvia_sms: false,
        notify_techn_v_email: false,
        notify_techn_v_sms: false,
        conditions: [],
        client_caller_id: null,

    }
}

function extractEmails(text) {
    let arr = []
    let emaillst = text.match(/([a-zA-Z0-9._+-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
    if (emaillst === null) {
        // no Email Address Found
        return []
    } else {
        const uniqueEmail = Array.from(new Set(emaillst));
        const finaluniqueEmail = [];
        for (let i = 0; i < uniqueEmail.length; i++) {
            let email = uniqueEmail[i]
            email = email ? email.toLowerCase() : email
            let characterIs = String(email).charAt(String(email).length - 1)
            if (characterIs === '.') {
                finaluniqueEmail.push(String(email.slice(0, -1)))
            } else {
                finaluniqueEmail.push(email);
            }
        }
        //emaillst = finaluniqueEmail.join('\n').toLowerCase();

        return finaluniqueEmail
    }

}

function getNotifications(od_sett) {
    let call = {}
    const { en_cont_list,
        notifyClientCallReceiptEmail = false, //potw przyjęcia
        notifyClientProtocolIssueEmail = false, //wysyłka protokołu
        notifyClientVisitPlannedEmail = false, //awizacja
        notifyTechncianOnCallReceiptEmail = false, //powiadomienie techika email
        notifyTechncianOnCallReceiptSMS = false, //powiadomienie technika SMS
    } = od_sett || {}
    call.client_call_recepit_email = notifyClientCallReceiptEmail || false; //o zleceniu
    call.client_notifyvia_email = notifyClientProtocolIssueEmail || false; //o protokolach
    call.client_visit_plan_email = notifyClientVisitPlannedEmail || false;
    call.notify_techn_v_email = notifyTechncianOnCallReceiptEmail || false;
    call.notify_techn_v_sms = notifyTechncianOnCallReceiptSMS || false;
    return call
}
async function findClientAndSiteByEmail(email) {
    if (!email) return {}
    const { fromEmail: sender_email, html, text, textAshtml, from_domain } = email || {}

    const emails_a = extractEmails(html + '')
    let emails = [...emails_a, sender_email]
    const cur_db = await db.queryBuilder().select(db.raw(`current_database() as dbname`)).first()

    //lista kontaktów dla emaili wyodrębnionych z emaila
    const contact_a = await db('customer_contacts').select(db.raw(`id,fname,lname, fname||' '||lname as name, email,telefon`))
        .whereRaw(`lower(trim(email))=ANY(:emails)`, { emails: emails })

    //szukanie w kontaktacvh oobiektu oraz w emailu obiektu
    const sitWh = `
    (id in (select site_id from site_2_contact where contact_id in (select  id from customer_contacts where lower(trim(email))=ANY(:emails)  )))
    OR (id in (select parent_id from s_assets_site s where s.site_email=ANY(:emails)))
    `
    //pierwszy na liscie obiekt gdzie najczesciej wystepują zlecenia
    const site_a = await db('s_site_erp').whereRaw(sitWh, { emails: emails })
        .select(db.raw(`id, erp_code, site_name, (select default_client_id from s_assets_site sa where sa.parent_id = s_site_erp.id ) as def_client_id`))
        .orderByRaw(`(select count(*)::int from se_service_calls c where c.site_id=s_site_erp.id ) desc `)
    //szukanie czy znaleziony obiekt ma domyslnego klienta - def_Client_id
    let site_o = site_a ? site_a[0] : {}
    const { id: site_id, def_client_id } = site_o || {}

    let fin_cli_id = null
    let client_o = null
    let enable_cli_imap
    if (!def_client_id) { //brak domyslnego klienta w obiekcie - szukam emaili wg kontaków klieta i tworzę listę klientów 
        const selCli = `id, erp_name, tax_id, customer,(select imap_read_enabled from cust_master_ext e where e.bp_id=business_partner_erp.id) as imap_read_enabled`
        const cliWh = `(id in ( select cust_mast_id from client_2_contact where contact_id in (select  id from customer_contacts where  lower(trim(email)) like ANY(:emails))))`
        const cliOrdBy = `(select count(*)::int from se_service_calls c where c.client_id=business_partner_erp.id ) desc  `
        const client_a = await db('business_partner_erp').select(db.raw(selCli)).whereRaw(cliWh, { emails: emails }).orderByRaw(cliOrdBy)

        if (client_a && client_a.length) {
            client_o = client_a[0]
            const { id, customer, imap_read_enabled } = client_o || {}
            fin_cli_id = id
            enable_cli_imap = imap_read_enabled
        }
    } else {//jest defaultowy klient - ustawiam client id do zlecenia
        fin_cli_id = def_client_id
    }
    return {
        contact_a: contact_a,
        client_o: client_o,
        site_o: site_o,
        site_id: site_id,
        client_id: fin_cli_id,
        is_client: !!fin_cli_id,
        cli_imap_enabled: enable_cli_imap
    }
}

async function SaveEmailInvoice2db(od_invoice, attachments, default_user = null) {
    const { id: user_id = null } = default_user || {}
    const { emailobject = null } = od_invoice || {}
    //zapis nowego dokumentu OD
    const newINVOICE = await saveInvoiceviaMainApi(od_invoice, { eid: user_id })
    const retemailSav = await saveEmail2fdoc_email(newINVOICE.id, emailobject)
    //zapis załączników do tego dokumentu
    const att_res = await Promise.map(attachments, function (attachmentObj) {
        return writeOD_File(newINVOICE.id, attachmentObj)
    }, { concurrency: 1 })
    return newINVOICE
}

async function SaveSE_Call_2_DB(se_call, em, attachments, default_user = null) {
    const { id: user_id = null } = default_user || {}
    const { contacts_a = [], ...rest } = se_call || {}
    const { fromEmail, date, email_uuid } = em || {}


    let dt = safeConvert2Date(date)
    const inDate = dt ? format(dt, 'DD.MM.YYYY HH:mm') : '?'

    //zapis nowego dokumentu zlecenia
    const newCall = await saveSECallViaMainApi(rest, { eid: user_id })
    const ID = newCall.id
    await saveCall2Email_Info(em, newCall)
    let logText = `Nowe zlecenie z emaila od ${fromEmail} z dn. ${inDate} utworzone.`
    await sav2SECallLog(ID, logText, user_id)

    //zapis załaczników do tego dokumentu
    const att_res = await Promise.map(attachments, function (attachmentObj) {
        return writeSECALL_File(ID, attachmentObj)
    }, { concurrency: 1 })

    return newCall
}
async function checkIfEmailSaved(email_uuid) {
    const ret = await db("email_2_calls_log").where('email_id', email_uuid).first()
    return ret
}
async function saveCall2Email_Info(em, newCall) {
    const c2e = email2callMap(newCall)
    c2e.email_id = em.email_uuid
    c2e.e_from = em.from
    c2e.e_subject = em.subject
    c2e.email_date = em.date
    c2e.e_to = em.to;
    const ret = await db("email_2_calls_log").insert(c2e, "id")
    const neID = ret[0].id;
    return neID
}
async function sav2SECallLog(call_id, inText, eid) {
    const o2db = { parent_id: call_id, date_added: new Date(), added_by: eid, event_text: inText }
    const ret = await db('se_service_calls_log').insert(o2db, "id")
}

function email2callMap(call_o,) {
    const { id, client_id } = call_o || {}
    const proto = {
        email_date: null,
        email_id: null,
        e_from: '',
        e_to: '',
        e_subject: '',
        call_id: id,
        client_id: client_id,
    }
    return proto;
}
async function findLEByTaxtId(tax_id_text) {
    const selLE = `id, vatid, entity_name, active, is_default`
    const whRaw = `lower(REPLACE(REPLACE (vatid, '-',''), ' ','')) = REPLACE(REPLACE (:taxid, '-',''), ' ','') `
    const ex = await db('fdoc_legal_ent').select(db.raw(selLE)).whereRaw(whRaw, { taxid: tax_id_text }).first();
    return ex
}
async function findBusinessPartnerByTaxId(tax_id_text) {
    const selLE = `id,erp_id, trim(erp_name) as erp_name, address1, address2 as zip_city, REPLACE(REPLACE (tax_id, '-',''), ' ','') as tax_id`
    const whRaw = `lower(REPLACE(REPLACE(REPLACE (tax_id, '-',''), ' ',''),'PL','')) = REPLACE(REPLACE(REPLACE (:taxid, '-',''), ' ',''),'PL','') `
    const ex = await db('business_partner_erp').select(db.raw(selLE)).whereRaw(whRaw, { taxid: tax_id_text }).first();
    return ex
}
async function findMPK4WBSTExt(wbstext) {
    const selLE = `id as mpk_id, wbs_id,segm_code
    ,(select id from fdoc_buinits b where lower(b.code)=lower(fdoc_mpk.segm_code)) as bu_id
    ,(select work_package_id from public.sap_projects_work_package w where w.id=fdoc_mpk.wbs_id) as wbs`
    const whRaw = `(wbs_id = (select id from sap_projects_work_package w where trim(w.work_package_id) =trim(:wbstext)))`
    const ex = await db('fdoc_mpk').select(db.raw(selLE)).whereRaw(whRaw, { wbstext: wbstext }).first();
    return ex
}

//przetworzenei otrzmanego JASNAn obiekt
// dane 100 % pewne bo sa z JSONA w emailu
async function getProcessInvoiceFromJSONMetaData(mail, user) {
    //znajdowanie pliku JSON z matadanymi faktury (jesli jest dołączony do emaila)
    const { attachments = [] } = mail || {}
    const json_file = (attachments || []).find(att => {
        const { filename = '', contentType = '' } = att || {}
        const is_json = filename.includes(`.json`) || contentType.includes(`json`)
        return is_json
    })
    let json_metatdata;
    if (json_file) {
        try {
            const { content } = json_file || {}
            json_metatdata = JSON.parse(content.toString());
        }
        catch (error) {
            console.log(`Buforowanie załącznika json ${json_file.filename}  ${json_file.contentType} nieudane`)
        }
    } else {
        return null
    }

    let odp = mapSalesInvoiceMeta2_ODdoc(json_metatdata)
    odp.date_received = new Date()
    odp.fa_date = safeConvert2Date(odp.fa_date)
    odp.fa_sales_date = safeConvert2Date(odp.fa_sales_date)

    //znalezienie spółki (adreat faktury) na podstawie NIP
    const foundLE = await findLEByTaxtId(odp.client_nip) //klient JSON == LE ww OD!
    odp.legal_ent_id = foundLE ? foundLE.id : null;

    //znalezienie dostawcy na podstawie NIP
    const foundVendor = await findBusinessPartnerByTaxId(odp.seller_nip) //dostawca wz bazy busines aprtner
    const vend_o = mapVendorObject(foundVendor)
    odp = { ...odp, ...vend_o }
    //szukam MPK_ID na podstawie textu WBS
    const foundWBS = await findMPK4WBSTExt(odp.client_wbs)
    if (foundWBS) {
        odp.mpk_id = foundWBS.mpk_id || undefined
        odp.bunit_id = foundWBS.bu_id || undefined;
        const akceptacjeArr = await getAcceptanceArray(odp, user)
        odp.akceptacje = akceptacjeArr || []
        if (odp.bp_o && odp.legal_ent_id && akceptacjeArr && akceptacjeArr.length > 0) {
            //są akceptacje więc status może być na do akceptacji
            odp.status_id = 200
        }
    }
    return odp;
}
//mapowanie JSONa ząłcznika na pola fdokumentu
function mapSalesInvoiceMeta2_ODdoc(s) {
    return {
        date_received: new Date(),
        fa_date: s.fa_date,
        fa_sales_date: s.fa_sales_date,
        fa_number: s.fa_number,
        client_nip: s.client_nip,
        seller_nip: s.seller_nip,
        fa_cost_month: s.month,
        fa_cost_year: s.year,
        net_value: s.netto,
        vat_amount: s.vat,
        gross_value: s.brutto,
        client_wbs: s.client_wbs,
        type: s.type === 'inv' ? 1 : null,
        dcomments: s.inv_title,
        cost_cat_id: s.cost_cat_id
    }
}
function mapVendorObject(bp_o) {
    if (!bp_o) return {}
    const { id, erp_name, address1, zip_city, erp_id, tax_id } = bp_o || {}
    return {
        bp_o: bp_o,
        b_partner_id: id,
        bp_name: erp_name,
        erp_name: bp_o.erp_name,
        bp_address: address1,
        bp_city: zip_city,
        erp_id: erp_id,
        tax_id: tax_id
    }
}

async function saveInvoiceviaMainApi(invoiceObj, jwtuser) {
    const { uuid = null, akceptacje = [] } = invoiceObj || {};
    let ret = { msg: '', status: 200, id: null }
    const o2d = mappers.map4db_fdoc_documents(invoiceObj)
    const ex = await db('fdoc_documents').where("uuid", uuid).first();
    let ID = null;
    if (!!ex) {
        ID = ex.id;
        delete o2d.uuid;
        delete o2d.id;
        delete o2d.date_added;
        await db('fdoc_documents').where("id", ex.id).update(o2d, "id");
        ret.msg = 'Dokument został zapisany.'
    } else {
        o2d.date_added = new Date();
        const res = await db('fdoc_documents').insert(o2d, "id");
        ID = res[0].id;
        ret = { ...ret, ...{ id: ID, status: 201, msg: 'Dokument został utworzony' } }
    }
    const saveAcc = await SyncUpdateAcceptanceObjects(ID, akceptacje, jwtuser)
    const inv = await db('fdoc_documents').where("id", ID).first();
    const dt = Date.now()
    console.log(`[Email reader] Saved fdoc invoice] on  ${dt} `, inv)
    return inv
}
async function saveSECallViaMainApi(call_o, jwtuser) {
    const { uuid = null, akceptacje = [] } = call_o || {};
    let ret = { msg: '', status: 200, id: null }
    const o2d = mappers.map4db_se_service_calls(call_o)
    const ex = await db('se_service_calls').where("uuid", uuid).first();
    let ID = null;
    if (!!ex) {
        ID = ex.id;
        delete o2d.uuid;
        delete o2d.id;
        delete o2d.date_registered;
        await db('se_service_calls').where("id", ex.id).update(o2d, "id");
        ret.msg = 'Dokument został zapisany.'
    } else {
        o2d.date_registered = new Date();
        const res = await db('se_service_calls').insert(o2d, "id");
        ID = res[0].id;
        ret = { ...ret, ...{ id: ID, status: 201, msg: 'Dokument został utworzony' } }
    }
    // const saveAcc = await SyncUpdateAcceptanceObjects(ID, akceptacje, jwtuser)
    const inv = await db('se_service_calls').where("id", ID).first();
    const dt = Date.now()
    console.log(`[Email SE CALLS] Saved se_call] on  ${dt} `, inv)
    return inv
}
async function testIfMAilAlreadyinOD(mail_uuid) {
    const whRaw = `trim(email_uuid)=:uuid`
    const odDoc = await db("fdoc_documents").select("id", "sender_email", "email_date", "email_uuid").whereRaw(whRaw, { uuid: mail_uuid }).first()
    return !!odDoc
}

async function testIf_SECALL_MailAlreadySaved(mail_uuid) {
    const o = await db("email_2_calls_log").where("email_id", mail_uuid).first()
    return !!o
}


async function SyncUpdateAcceptanceObjects(faktura_id, acceptanceArray, jwtuser) {
    let ret_cond_ids_arr = [];
    const db_entries = await db("fdoc_acceptance").where("invoice_id", faktura_id);
    const step1 = await Promise.map(db_entries, (dbacc) => {
        const exisinpayload = acceptanceArray.find(acc => {
            return acc.uuid === dbacc.uuid
        })
        if (!exisinpayload) {
            return db("fdoc_acceptance").where("id", dbacc.id).delete()
        }
    })
    return Promise.map(acceptanceArray, (accObj) => {
        return UpdateAcceptRow(faktura_id, accObj, jwtuser).then(function (rr) {
            ret_cond_ids_arr.push(rr);
            return rr;
        })
    }).then(function (res) {
        return ret_cond_ids_arr
    })
}
async function getAcceptanceArray(invoice, user) {
    const { mpk_id = null, net_value = 0, bunit_id = null } = invoice || {};
    const mpkDB = await getMPKwExceptions(mpk_id) //matryca akceptacji
    const { kob = null, okr = null, dyr = null, mgr4okr = null, mgr4dyr = null, segm_code = '' } = mpkDB || {};

    const acc_array = []

    const lev_1 = getNewAcceptTempl(user, kob, 1);
    //szukanie 2 akceptującego
    let acceptorL2 = null
    if (kob.id !== okr.id) //wstawianei OKR lub powyżej jeśli pusty (bardzo nieprawdopodobne ale...)
        acceptorL2 = okr ? okr : (dyr ? dyr : mgr4okr ? mgr4okr : mgr4dyr ? mgr4dyr : null)
    else {
        //KOB=OKR  to sytuacja zabroniona - szukanie szefa
        acceptorL2 = dyr ? dyr : mgr4dyr ? mgr4dyr : null
    }

    let acceptorL3 = null;
    if (acceptorL2.id !== dyr.id)
        acceptorL3 = dyr ? dyr : mgr4dyr
    else
        acceptorL3 = mgr4dyr || null;

    const lev_2 = getNewAcceptTempl(user, acceptorL2, 2)
    //const lev_3 = getNewAcceptTempl(user, acceptorL3, 3);

    if (lev_1) acc_array.push(lev_1)
    if (lev_2) acc_array.push(lev_2)

    //tutaj nie ma wartości (na tym etapie) więc nei ma sensu szukac 3 ciego akceptującego ...
    // const bu = await db('fdoc_buinits').where("code", segm_code).first(); //
    //const { accept_levels = null, accept_max_level = 99999 } = bu || {}; //pobiranie ilosci poziomóœ akceptacji
    // const add3rdAccept = accept_levels && (net_value > accept_max_level) && (accept_levels === 3)
    // if (add3rdAccept && lev_3)
    //     acc_array.push(lev_3)
    return acc_array
}
async function UpdateAcceptRow(invoice_id, acceptObject, jwtUsr) {
    const { uuid = null, decision_o = null } = acceptObject;
    const { eid = null } = jwtUsr;
    if (!uuid) return { success: false, uuid: uuid, id: null }
    const faktura = await db("fdoc_documents").select("added_by", "status_id").where("id", invoice_id).first()
    if (!faktura) throw new Error("Nie znaleziono faktury o podanym identyfikatorze!!")

    if (faktura.is_booked) {
        throw new Error("Faktura została zaksiegowana. Akceptacja nie jest już możliwa!")
    }
    //odkodowanie akceptacji (obiekt!)
    const exist = await db("fdoc_acceptance").where("uuid", uuid).first();
    const acc4db = mappers.map4db_fdoc_acceptance(acceptObject);
    acc4db.invoice_id = invoice_id;

    if (!acc4db.acceptor_id)
        acc4db.acceptor_id = eid;
    if (!acc4db.acceptor_decision_date)
        acc4db.acceptor_decision_date = new Date();
    delete acc4db.id;
    let ID = null;
    if (exist) {
        ID = exist ? exist.id : null;
        if (!acc4db.added_by)
            acc4db.added_by = eid
        else
            delete acc4db.added_by;
        delete acc4db.uuid;
        const test = await db("fdoc_acceptance").where("id", ID).update(acc4db, "id")
    }
    else {
        acc4db.dateadded = new Date();
        acc4db.added_by = eid;
        const ins = await db("fdoc_acceptance").insert(acc4db, "id")
        ID = ins[0] ? ins[0].id : null
    }
    return { success: true, uuid: uuid, id: ID }
}
async function getMPKwExceptions(id) {
    const sel = ` m.id, (select work_package_id from sap_projects_work_package w where w.id = m.wbs_id) as erp_wbs
    , (select work_package_name from sap_projects_work_package w where w.id = m.wbs_id) as erp_wbs_name
        , m.segm_code, (not e.kob_id is null) and(e.is_active) as has_except
            , CASE WHEN(not e.kob_id is null) and(e.is_active)
    THEN e.kob_id  ELSE m.kob_id  END as kob_id
        , CASE WHEN(not e.okr_id is null) and(e.is_active)
    THEN e.okr_id
    ELSE(select okr_id from fdoc_kob_2_okr k where k.kob_id = m.kob_id limit 1)
    END as okr_id
        , CASE WHEN(not e.dyr_id is null) and(e.is_active)
    THEN e.dyr_id
    ELSE(select dyr_id from fdoc_okr_2_dyr k where k.okr_id = (select okr_id from fdoc_kob_2_okr k where k.kob_id = m.kob_id limit 1) limit 1 )
    END as dyr_id`

    const mpk = await db.queryBuilder().from('fdoc_mpk as m').leftJoin('fdoc_mpk_exceptions as e', 'm.id', 'e.parent_id')
        .where(db.raw(`m.id =:id`, { id: id }))
        .select(db.raw(sel)).first()

    if (!mpk) return {}
    const kob = await getEmployeeWManager(mpk.kob_id);
    const okr = await getEmployeeWManager(mpk.okr_id);
    const dyr = await getEmployeeWManager(mpk.dyr_id);

    let manager4okr = null
    let manager4dyr = null;
    if (okr)
        manager4okr = await getEmployeeWManager(okr.manager_id);

    if (dyr)
        manager4dyr = await getEmployeeWManager(dyr.manager_id);


    mpk.kob = kob || {};
    mpk.okr = okr || {}
    mpk.dyr = dyr || {};
    mpk.mgr4okr = manager4okr || null
    mpk.mgr4dyr = manager4dyr || null
    return mpk;
}
async function saveEmail2fdoc_email(invoice_id, emailInfo) {
    if (!emailInfo) return
    const { from = '', to = '', from_domain = '', date = '', subject = '', html = '', text = '', textAshtml = '', email_uuid = '', att_info = '' } = emailInfo || {}
    const m2d = {
        parent_id: invoice_id,
        date_received: date,
        m_from: from,
        m_to: to,
        m_subject: subject,
        m_text: text,
        m_html: html,
        m_attachments: att_info,
        email_uuid: email_uuid,
    }
    try {
        const ins_res = await db('fdoc_emails').insert(m2d, "id")
        return { id: ins_res[0].id }
    }
    catch (error) {
        console.log(`SAVE to fdoc_emails FAILED: `, error)
    }

}
async function getEmployeeWManager(id) {
    const sql = `id, fname, lname, fname || ' ' || lname as "name", department_id, phone, email, stanowisko, manager_id, (select  fname || ' ' || lname from se_employees ee where ee.id = se_employees.manager_id) as manager,
    (manual_block = true) OR(coalesce(is_active, false) = false) as blokada `
    return db('se_employees').where('id', id).select(db.raw(sql)).first()
}
async function cli_GetBusinessPartner(id) {
    const sq = `id, erp_id, tax_id, pay_term, erp_name, address1, address2, ad_hoc,
    coalesce((select  efaktura from cust_master_ext ex where ex.bp_id = business_partner_erp.id limit 1), false) as efaktura,
        coalesce((select  efaktura_email from cust_master_ext ex where ex.bp_id = business_partner_erp.id limit 1), '') as efaktura_email
            , (SELECT bool_or(unpaid) as has_unpaid  FROM attachment_4_invoices_4_calls
where parent_id in (select id id from se_service_calls c where c.client_id = business_partner_erp.id) ) as has_unpaid
    , (SELECT bool_or(pay_alert) as has_alert  FROM attachment_4_invoices_4_calls
where parent_id in (select id id from se_service_calls c where c.client_id = business_partner_erp.id) ) as has_alert
    `
    const bp = await db('business_partner_erp').select(db.raw(sq)).where('id', id).first();
    return bp
}
async function writeSECALL_File(doc_id, attachment) {
    const { content: contentBuffer = null, contentType = '', filename = '', size = null, contentDisposition = '', partId = null } = attachment || {}
    const contentParts = (contentType || '').split('/')
    const subtype = contentParts ? contentParts[1] : 'def'
    let _fname = filename ? filename : 'default.' + subtype
    _fname = _fname.replace(/\//g, '\u2215'); //usuwanie slashy z naz- zamiana na UNICODE DIVIDER
    _fname = _fname.replace(/\s+/g, '_');
    const fsafe = sanitize(_fname);
    const filename_decoded = libmime.decodeWords(fsafe)//dekodowanie nazw mime encoded
    try {
        let fi = createFileInfoObj(filename_decoded, size);
        await writeFileAsync(fi.fullPath, contentBuffer);//zapis na dysk
        await addSeCallAttachment(doc_id, fi)//zapis w tabeli linkującej do OD
        return filename_decoded
    }
    catch (err) {
        console.error(err)
    }
}


async function writeOD_File(doc_id, attachment) {
    const { content: contentBuffer = null, contentType = '', filename = '', size = null, contentDisposition = '', partId = null } = attachment || {}
    const contentParts = (contentType || '').split('/')
    const subtype = contentParts ? contentParts[1] : 'def'
    let _fname = filename ? filename : 'default.' + subtype
    _fname = _fname.replace(/\//g, '\u2215'); //usuwanie slashy z naz- zamiana na UNICODE DIVIDER
    _fname = _fname.replace(/\s+/g, '_');
    const fsafe = sanitize(_fname);
    const filename_decoded = libmime.decodeWords(fsafe)//dekodowanie nazw mime encoded
    try {
        let fi = createFileInfoObj(filename_decoded, size);
        await writeFileAsync(fi.fullPath, contentBuffer);//zapis na dysk
        await addDocInvoiceAttachment(doc_id, fi)//zapis w tabeli linkującej do OD
        return filename_decoded
    }
    catch (err) {
        console.error(err)
    }
}
function createFileInfoObj(fname, size) {
    let fi = GetFileInfoTemplate();
    let folder = process.env.UPLOAD_PATH // conf.upload.uploadpath_prod;
    fi.file_name = fname;
    fi.file_size = size;
    fi.storage_folder = folder;
    fi.file_pointer = cl_filenameGenerator(fname); //tworzy nazwę ze stemplem czasu
    fi.fullPath = fi.storage_folder + fi.file_pointer
    fi.file_extension = getExtention(fname)
    return fi;
}
function cl_filenameGenerator(fileName) {
    return Date.now() + '..' + fileName
}
function getExtention(filename) {
    if (!filename) return '';
    const parts = filename.split('.');
    if (parts && parts.length > 0) {
        const last = parts[parts.length - 1]
        return last || '';
    } else {
        return ''
    }
}
function GetFileInfoTemplate() {
    return {
        file_pointer: "",
        file_name: "",
        file_extension: "",
        file_size: null,
        storage_folder: "",
        added_by_id: null
    }
}
async function addDocInvoiceAttachment(invoice_id, fileInfo) {
    const select = `  file_id as file_id, file_id as id, id as link_id, (SELECT file_name from file_attachments fa where fa.id = file_id) as file_name
    , (SELECT date_added from file_attachments fa where fa.id = file_id) as date_added
        , (SELECT file_size from file_attachments fa where fa.id = file_id) as file_size
            , (SELECT file_size / 1024 from file_attachments fa where fa.id = file_id) as file_size_kb
                , '' as parent_id `
    const insFileID = await InsertFileInf2Db(fileInfo);
    const linkRow = { parent_id: invoice_id, file_id: insFileID }
    const linkRet = await db("attachment_2_doc_invoice").insert(linkRow, "id")
    const linkID = linkRet[0].id;
    const newFile = await db("attachment_2_doc_invoice").select(db.raw(select)).first().where("id", linkID);
    return newFile
}
async function addSeCallAttachment(call_id, fileInfo) {
    const select = `
    file_id as file_id, file_id as id, id as link_id
    , (SELECT file_name from file_attachments fa where fa.id = file_id) as file_name
    , (SELECT date_added from file_attachments fa where fa.id = file_id) as date_added
    , (SELECT file_size from file_attachments fa where fa.id = file_id) as file_size
    , (SELECT file_size / 1024 from file_attachments fa where fa.id = file_id) as file_size_kb
    , call_id as parent_id
     `
    const insFileID = await InsertFileInf2Db(fileInfo);
    const linkRow = { call_id: call_id, file_id: insFileID }
    const linkRet = await db("se_service_calls_2_attachments").insert(linkRow, "id")
    const linkID = linkRet[0].id;
    const newFile = await db("se_service_calls_2_attachments").select(db.raw(select)).first().where("id", linkID);
    return newFile
}
async function InsertFileInf2Db(fi) {
    const f2db = mapFi2Db(fi);
    delete f2db.id;
    f2db.date_added = new Date();
    const ret = await db("file_attachments").insert(f2db, "id")
    return ret[0].id;
}
function mapFi2Db(fi) {
    return {
        id: fi.id,
        file_pointer: fi.file_pointer,
        file_name: fi.file_name,
        file_extension: fi.file_extension,
        storage_folder: fi.storage_folder,
        date_added: fi.date_added,
        added_by_id: fi.added_by_id,
        file_size: fi.file_size,
        category_id: fi.category_id
    }
}
function getNewAcceptTempl(user, manager, step) {
    if (!manager) return null;
    return {
        uuid: fuid(),
        added_by: user.id,
        dateadded: new Date(),
        invoice_id: null,
        akceptor: manager,
        acceptor_id: manager ? manager.id : null,
        accepted: null,
        acceptor_decision_date: null,
        remarks: 'auto',
        step: step
    }
}
async function getGetAllSetups() {
    const globSett = await setup.getDeepSettings()
    const { e_dinvoices = false } = globSett || {};
    const setupOD = await setup.getSetting4DInvoices()
    const { od_imp_w_attachm = false, enableImapDInvoice = false, days_back2check = 1, checkIntervalMinutes = 15, subjectIgnoreKeywords = '', subjectKeywords2Accept = '', def_user = null } = setupOD || {};
    return {
        enable_globalOD: e_dinvoices, monitorEmails: enableImapDInvoice, days_back2check: days_back2check,
        checkIntervalMinutes: checkIntervalMinutes,
        subjectKeywords2Accept: subjectKeywords2Accept,
        subjectIgnoreKeywords: subjectIgnoreKeywords,
        KEYWORDS_ACC: ConvertKeyWString_2_Array(subjectKeywords2Accept),
        KEYWORDS_IGN: ConvertKeyWString_2_Array(subjectIgnoreKeywords),
        def_user: def_user,
        od_settings: setupOD,
        import_only_mails_w_att: od_imp_w_attachm
    }
}
async function getSetup4ServiceIncoming() {
    const set = await setup.getOptions4Zlecenia()
    const { enableImapIntegration, days_back2check, alwaysNotifyEmail2ClientOnIMAPCallRegist, alwaysSendProtocolEmail2ClientOnIMAPCallRegist,
        emailSubjectKeywords4CallDetect, emailSubjectIgnoreKeywords, secall_imp_w_attachm, def_user } = set || {}
    return {
        monitorEmails: enableImapIntegration,
        days_back2check: days_back2check || 1,
        //checkIntervalMinutes: checkIntervalMinutes,
        subjectKeywords2Accept: emailSubjectKeywords4CallDetect,
        subjectIgnoreKeywords: emailSubjectIgnoreKeywords,
        KEYWORDS_ACC: ConvertKeyWString_2_Array(emailSubjectKeywords4CallDetect),
        KEYWORDS_IGN: ConvertKeyWString_2_Array(emailSubjectIgnoreKeywords),
        import_only_mails_w_att: secall_imp_w_attachm,
        def_user: def_user,
        call_settings: set,
    }


}

function ConvertKeyWString_2_Array(keywords_sting) {
    let KEYWords = (keywords_sting || '').split(",").map(s => s.toLowerCase()).map(s => s.trim()).filter(word => !!word);
    return KEYWords
}
async function getActiveMailboxes() {
    const whRaw = `(enabled=true) AND (use_4_receive = true) and (use_4_od = true)`
    const boxes = await db('app_mailboxes').select(db.raw(`* `)).whereRaw(whRaw)
    return boxes
}
async function getActiveMailboxes4ServiceCalls() {
    const whRaw = `(enabled=true) AND (use_4_receive = true) AND (use_4_servcalls = true)`
    const boxes = await db('app_mailboxes').select(db.raw(`* `)).whereRaw(whRaw)
    return boxes
}
async function checkAndFillField(field, date_pocz, sender_email, detection_level_procent, invoice) {
    const stats = await getStats4Field(field, date_pocz, sender_email)
    if (!stats || stats.length === 0) return
    if (!detection_level_procent) return
    const lev = detection_level_procent * 1;
    if (lev <= 0 || lev > 100) return

    const firstRow = stats[0]
    const { share = null } = firstRow || {}
    //wpisanie wartoci pola jesli poziom ufnoci został osiagnięy
    if (share >= lev) {
        const fieldValue = firstRow[field]
        if (fieldValue) {
            invoice[field] = fieldValue
            console.log('[OD_autofill]Pole: ' + field, fieldValue)
        }
    } else {
        console.log(`[OD_autofill]Share too low for: ${field}; val: ${share} `)
    }
}
async function getStats4Field(groupField, date_pocz, sender_email) {
    let _filter = `(date_added >=:date_pocz) and(trim(lower(sender_email)) = trim(lower(:email)))`
    const fltr = { date_pocz: date_pocz, email: sender_email }
    //statystyki tylko oparte na rekordach gdzie badane pole nie jest null!!!
    let innerFilter = _filter + ` and (${groupField} is not null)`
    const innerQuery = db('fdoc_documents').select(db.raw(groupField + `, count(*) as cnt, sum(count(*)) OVER() as total`))
        .whereRaw(innerFilter, fltr).groupBy(groupField).orderByRaw(`count(*) desc`).as('x')

    const outerQuery = await db.queryBuilder().select(db.raw(`*, (100 * cnt / total) as share`)).from(innerQuery)
    return outerQuery
}

function safeConvert2Date(in_datetime) {
    //na wejściu data lub string daty  - na wyjsciu zawsze data
    if (in_datetime === null || in_datetime === undefined)
        return null
    let s_dt = null;
    if (typeof in_datetime === 'string') { //zamiana łacucha na datę
        s_dt = parse(in_datetime);
    } else {
        s_dt = in_datetime //bez konwersji
    }
    if (isValid(s_dt))
        return s_dt
    else
        return null
}
